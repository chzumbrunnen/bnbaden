<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION

// replace delightful downloads button with bnbaden custom buttons
// see: https://delightfuldownloads.com/documentation/custom-buttons/

function dedo_custom_button( $buttons ) {
    $custom_buttons['theme_default'] = array(
        'name' => __( 'Theme Default', 'delightful-downloads' ),
        'class' => 'creativ-shortcode creativ-shortcode-button creativ-shortcode-button-colour-theme creativ-shortcode-button-size-medium creativ-shortcode-button-edge-straight'
    );
    return $custom_buttons;
}
add_filter( 'dedo_get_buttons', 'dedo_custom_button' );

register_sidebar(array(
	'name' => __( 'Registration', 'cr3_attend_theme' ),
	'id' => 'registration',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h5>',
    'after_title' => '</h5>'
));